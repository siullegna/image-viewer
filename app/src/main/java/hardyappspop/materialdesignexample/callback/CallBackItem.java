package hardyappspop.materialdesignexample.callback;

/**
 * Created by luisangelgarcia on 6/29/15.
 */
public interface CallBackItem {
    void onClickListener(int position);
    void onLongClickListener(int position);
}
