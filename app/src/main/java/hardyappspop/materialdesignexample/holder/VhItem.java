package hardyappspop.materialdesignexample.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import hardyappspop.materialdesignexample.R;
import hardyappspop.materialdesignexample.callback.CallBackItem;

/**
 * Created by luisangelgarcia on 6/29/15.
 */

public class VhItem extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    public FrameLayout frameItem;
    public ImageView img;
    public TextView txt;
    public LinearLayout ll;
    private CallBackItem callBackItem;
    // ===========================================================
    // Constructors
    // ===========================================================

    public VhItem(View itemView, CallBackItem callBackItem) {
        super(itemView);
        this.callBackItem = callBackItem;

        frameItem = (FrameLayout) itemView.findViewById(R.id.frame_item);
        img = (ImageView) itemView.findViewById(R.id.img);
        txt = (TextView) itemView.findViewById(R.id.txt);
        ll = (LinearLayout) itemView.findViewById(R.id.ll);

        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public void onClick(View v) {
        callBackItem.onClickListener(getAdapterPosition());
    }

    @Override
    public boolean onLongClick(View v) {
        callBackItem.onClickListener(getAdapterPosition());
        return false;
    }

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
